import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { performFilteredSearch } from '../actions/index.js';

import { Grid, Row, Col } from 'react-flexbox-grid';


const MIN_PRICE = 'MIN_PRICE';
const MAX_PRICE = 'MAX_PRICE';
const MIN_ALCOHOL = 'MIN_ALCOHOL';
const MAX_ALCOHOL = 'MAX_ALCOHOL';


class FilterForm extends Component {
	constructor(props){
		super(props);
		this.state = {
			min_price: '',
			max_price: '',
			min_alcohol: '',
			max_alcohol: '',
		}
		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.renderInput = this.renderInput.bind(this);
		this.termChanged = this.termChanged.bind(this);
	}

	onFormSubmit(event){
		event.preventDefault();
		console.log('Submitting form');
		this.props.performFilteredSearch(
			this.state.min_price,
			this.state.max_price,
			this.state.min_alcohol,
			this.state.max_alcohol);

	}

	renderInput(type){
		switch(type){
			case MIN_PRICE:
				return (
						<div className='form-group'>
							<label htmlFor='min_price_input'>Min price</label>
							<input
							id='min_price_input'
							value={this.state.min_price}
							onChange={event => this.termChanged(event.target.value, MIN_PRICE)}
							type='number'
							className='form-control' />
						</div>
					);
			case MAX_PRICE:
				return (
						<div className='form-group'>
							<label htmlFor='max_price_input'>Max price</label>
							<input
							id='max_price_input'
							value={this.state.max_price}
							onChange={event => this.termChanged(event.target.value, MAX_PRICE)}
							type='number'
							className='form-control' />
						</div>
					);
			case MIN_ALCOHOL:
				return (
						<div className='form-group'>
							<label htmlFor='min_alcohol_input'>Min alcohol</label>
							<input
							id='min_alcohol_input'
							value={this.state.min_alcohol}
							onChange={event => this.termChanged(event.target.value, MIN_ALCOHOL)}
							type='number'
							className='form-control' />
						</div>
					);
			case MAX_ALCOHOL:
				return (
						<div className='form-group'>
							<label htmlFor='max_alcohol_input'>Max alcohol</label>
							<input
							id='max_alcohol_input'
							value={this.state.max_alcohol}
							onChange={event => this.termChanged(event.target.value, MAX_ALCOHOL)}
							type='number'
							className='form-control max_alcohol_input' />
						</div>
					)
		}
	}

	termChanged(value, type){
		switch(type){
			case MIN_PRICE:
				this.setState({min_price:value});
				break; // break stmnt is needed in order for all fields not gaining the same values
			case MAX_PRICE:
				this.setState({max_price:value});
				break;
			case MIN_ALCOHOL:
				this.setState({min_alcohol:value});
				break;
			case MAX_ALCOHOL:
				this.setState({max_alcohol:value});
				break;
		}
	}

	render(){
		return(
				<div className='form-group filter-form'>
					<form onSubmit={this.onFormSubmit} className='input-group'>
						<Grid fluid>
							<Row>
								<Col xs={6} md={6}>
									{this.renderInput(MIN_PRICE)}
									{this.renderInput(MAX_PRICE)}
								</Col>
								<Col xs={6} md={6}>
									{this.renderInput(MIN_ALCOHOL)}
									{this.renderInput(MAX_ALCOHOL)}
								</Col>
							</Row>
						</Grid>
						<button type='submit' className='btn btn-default'>Submit</button>
					</form>
				</div>
			);
	}
}

					/*<div>
					{JSON.stringify(this.props.filteredResponse)}
					</div>*/


function mapStateToProps(state){
	return {
		filteredResponse:state.filteredResponse
	}
}

function mapDispatchToProps(dispatch){
	return 	bindActionCreators({performFilteredSearch:performFilteredSearch},dispatch);
}


export default connect(mapStateToProps,mapDispatchToProps)(FilterForm);