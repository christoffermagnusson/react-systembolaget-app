import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Grid, Row, Col } from 'react-flexbox-grid';


const headers = ['Name','Price','Volume','Alcohol %','Nr (id)', 'Type'];

class ResultsTable extends Component{
	constructor(props){
		super(props);

		this.renderRow = this.renderRow.bind(this);
		this.renderColumn = this.renderColumn.bind(this);
	}


	renderColumn(key, value){ // xs= and md= decides how many columns (up to 12) should be taken up by a column
		let columnWidth = (12/headers.length); // divides max columncount with nr of headers for all columns to fit screen
		let columnKey = `${key}:${value}`;
		return (
				<Col key={columnKey} xs={columnWidth} md={columnWidth}>
				{value}
				</Col>
			)
	}

	renderRow(rowData){
		console.log('Rendering row data = '+rowData.name);
		let attributes = [rowData.name, `${rowData.price} SEK`, `${rowData.volume} ml`, `${rowData.alcohol} %`, rowData.nr, rowData.product_group];
		return (
				<Row key={rowData.nr} className='data-row'>
				{attributes.map(attribute => this.renderColumn(rowData.nr,attribute))}
				</Row>
			)
	}


	render(){
		return (
				<Grid>
					<Row className='header-row'>
					{headers.map(header => this.renderColumn(header,header))}
					</Row>
					{this.props.filteredResponse.map(rowData => this.renderRow(rowData))}
				</Grid>
			)
	}
}






function mapStateToProps(state){
	return {
		filteredResponse:state.filteredResponse,
	}
}

export default connect(mapStateToProps)(ResultsTable);

