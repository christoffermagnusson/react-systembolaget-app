import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CHART_TYPES } from './types_utils.js';

const BarChart = require('react-chartjs').Bar;


class ItemChart extends Component{
	constructor(props){
		super(props);

		this.renderChart = this.renderChart.bind(this);
		this.sortMap = this.sortMap.bind(this);
	}

	sortMap(unsortedMap){
		let sortArray = [];
		for(let item of unsortedMap){
			sortArray.push(item);
		}
		sortArray.sort((x, y) => x[1] < y[1]);
		return new Map(sortArray);
	}

	renderChart(data){
		let chartDataKeys = [];
		let chartDataValues = [];
		let unsortedMap = data.map(value => value.product_group)
								  .reduce((valuesMap, value) => valuesMap.set(value, 1 + (valuesMap.get(value) || 0))
								  , new Map());

		let sortedMap = this.sortMap(unsortedMap);
		sortedMap.forEach((value, key, map) => {
			chartDataKeys.push(key);
			chartDataValues.push(value);
		})

		let chartData = {
			labels: chartDataKeys,
			datasets: [
				{
					label:'Types',
					fillColor: "rgba(45, 156, 215, 0.75)",
					strokeColor: "rgba(220,220,220,0.8)",
					highlightFill: "rgba(220,220,220,0.75)",
					highlightStroke: "rgba(220,220,220,1)",
					data:chartDataValues
				}
			]
		}
		let options = {
			scaleShowGridLines:true,
			scaleGridLineColor:"rgba(0,0,0,0,.05)",
		}

		return (
				<BarChart data={chartData} options={options} width='600' height='350' />
			)
	}

	render(){
		return(
			<div className='container-chart'>
				{this.renderChart(this.props.filteredResponse)}
			</div>
			)
	}
}


function mapStateToProps(state){
	return{
		filteredResponse:state.filteredResponse,
	}
}

export default connect(mapStateToProps)(ItemChart);