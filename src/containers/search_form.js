import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { performSearchByName } from '../actions/index.js';


class SearchForm extends Component{
	constructor(props){
		super(props);
		this.state = {
			term:'',
		}

		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.onTermChanged = this.onTermChanged.bind(this);
	}

	onFormSubmit(event){
		event.preventDefault();
		this.props.performSearchByName(this.state.term);
	}

	onTermChanged(term){
		this.setState({term});
	}

	render(){
		return(
			<div className='input-group-container'>
				<label htmlFor='search-input'>Search by name</label>
				<form onSubmit={this.onFormSubmit} className='input-group' id='search-input'>
					<input
					value={this.state.term}
					onChange={event => this.onTermChanged(event.target.value)}
					type='text'
					className='form-control' />
					<span className='input-group-btn'>
						<button type='submit' className='btn btn-default-search'>Search</button>
					</span>
				</form>
			</div>
		)
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({performSearchByName:performSearchByName}, dispatch)
}


export default connect(null, mapDispatchToProps)(SearchForm);