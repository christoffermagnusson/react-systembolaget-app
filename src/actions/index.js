import axios from 'axios';


const BASE_URL_FILTERED = 'http://localhost:8080/search/products/all?';
export const SEARCH_PERFORMED = 'SEARCH_PERFORMED';

export function performFilteredSearch(min_price, max_price, min_alcohol, max_alcohol){
	let queryUrl = `${BASE_URL_FILTERED}min_price=${min_price}&max_price=${max_price}&min_alcohol=${min_alcohol}&max_alcohol=${max_alcohol}`;
	console.log(`queryUrl : ${queryUrl}`);
	let request = axios.get(queryUrl);

	return {
		type: SEARCH_PERFORMED,
		payload:request,
	}
}

const BASE_URL_BY_NAME = 'http://localhost:8080/search/products/by-name?';

export function performSearchByName(name){
	let queryUrl = `${BASE_URL_BY_NAME}name=${name}`;
	console.log(queryUrl);
	let request = axios.get(queryUrl);

	return {
		type: SEARCH_PERFORMED,
		payload:request,
	}
}




// Sample api request
// http://localhost:8080/search/products/all?min_price=10&max_price=100&min_alcohol=5&max_alcohol=15
// min_price, max_price, min_alcohol and max_alcohol are the basic filtering attributes

// Sample response
/*[
    {
    "name": "Engelholms Pacific Pilsner",
    "price": 21.00,
    "volume": 330,
    "alcohol": 5.00,
    "nr": 3067603,
    "product_group": "Öl"
  }
]*/