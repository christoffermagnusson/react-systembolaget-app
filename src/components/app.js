import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';

import FilterForm from '../containers/filter_form.js';
import SearchForm from '../containers/search_form.js';
import ResultsTable from '../containers/results_table.js';
import ItemChart from '../containers/item_chart.js';


export default class App extends Component{
	constructor(props){
		super(props);
	}

	render(){
		return(
				<div>
					<div className='header-container'>
						<h1>Systembolaget filtering application</h1>
					</div>
					<Grid fluid>
						<Row>
							<Col xs={6} md={6}>
								<FilterForm />
							</Col>
							<Col xs={6} md={6}>
								<SearchForm />
								<ItemChart />
							</Col>
						</Row>
					</Grid>
					<ResultsTable />
				</div>
			)
	}
}


// Should consist of basic filtering form + eventual search field
// Table to display the different items
// Specialized views for different items?
