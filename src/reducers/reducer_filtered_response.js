import { SEARCH_PERFORMED, CLEAR_RESULTS } from '../actions/index.js';


export default(state = [], action) => {
	switch(action.type){
		case SEARCH_PERFORMED:
			return action.payload.data;
		default:
			return state;
	}
}