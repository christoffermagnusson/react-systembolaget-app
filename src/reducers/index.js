import { combineReducers } from 'redux';

import FilteredResponse from './reducer_filtered_response.js';

const rootReducer = combineReducers({
	filteredResponse:FilteredResponse
});


export default rootReducer;