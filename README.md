# react-systembolaget-app

* Web client app used to search and filter products from Systembolagets API. The client is built in React and uses Redux to handle state. Furthermore the app uses chart.js library to 
* display a chart of the results filtered or searched for. The backend consists of a Java EE Servlet (Winstone) and the data is stored in an SQlite database